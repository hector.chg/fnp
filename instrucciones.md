# Qué es y cómo usar git y virtualenv:
La idea de este tutorial es aprender qué es y cómo usar estas dos cosas. Primero haremos una carpeta donde trabajar, haremos un repositorio nuestro en gitlab y lo clonaremos en nuestro pc. 

Además, asumiendo que estaremos trabajando en python, montaremos un entorno virtual en nuestra carpeta de trabajo y haremos que NO se suba a nuestro server de git; puesto que ocupa un montón de espacio. En git sólo subiremos cosas que requieren control de versión; por ejemplo código o documentación. Nunca subiremos datos, imágenes, etc.

Usaremos gitlab y no github porque github es de microsoft y gitlab es open source, **fin**.

## Qué son git y virtualenv?
### Git
Git es un programa de terminal que asienta

### Virtualenv

----

## Cómo usar git y virtualenv?

### 1) Hazte cuenta en [GitLab](http://www.gitlab.com "GitLab")
Y agrégame como amigo o sígueme o como sea xD: _@hector.chg_ (_@hector-chg-data_ en GitHub)

### 2) Instala git en tu pc
Abre la terminal.

Presuponiendo debian-based y que no tienes git: `sudo apt-get install git`.

Si no tienes debian-based distro (o mac, o windows), dímelo y suerte con ello.

### 3) Clona el repositorio en tu espacio de trabajo

1) Haz una carpeta de proyectos en tus documentos y muévete dentro de ella:

```
cd ~/Documents
mkdir Proyectos
cd Proyectos
```

2) Ahora, dentro de _Proyectos_, clona el repositorio que has creado en _GitLab_ antes. En mi caso mi proyecto es el lab de nuclear. 
`git clone https://gitlab.com/hector.chg/fnp.git`

Al ejecutar `git clone` se hace la carpeta del proyecto.

Ésta URL se encuentra en un desplegable en la página principal de tu proyecto, en GitLab. Seguro que la encuentras, y si no, me dices.

### 4) Instala y activa el entorno virtual
Virtualenv te permite condensar el entorno de programación en un sólo fichero _requirements.txt_, que se sube a git. 

Este fichero tiene la lista de todas las librerías que se necesitan para el proyecto.

Los colaboradores del proyecto se instalan las librerías que hay en este fichero (punto 4).

1) Se instala: `pip install virtualenv`

2) Se hace: `virtualenv venv-FNP`

3) Se activa: `source venv-FNP/bin/activate`



### 5) Instala las dependencias para que todos los usuarios tengan librerías compatibles
La manera **guarra** de meter tus librerías en un archivo de texto es (con el entorno virtual ya activado): 

`pip freeze > requirements.txt`

Éste comando mete el output de `pip freeze` al fichero `requirements.txt`. Si no existe lo crea.

Es la manera guarra porque hay librerías que tienen dependencias entre sí y si ejecutas ese comando no meterás solo las librerias útiles sino una retaila de librerias no relevantes, porque pip ya se pega por ti para encontrar esas librerías.

Cuando alguien quiera colaborar contigo, que active su propio entorno virtual (`source ENTORNOVIRTUAL/bin/activate`) e instale recursivamente las librerías que hay dentro de `requirements.txt` (`pip install -r requirements.txt`).

### 6) Empezar a programar en spyder:
Para abrir el mejor IDE de python: teclea `spyder3`

TO-DO:
- como usar spyder
- por quú spyder3 para abrir el spyder 4


## EXTRAS:
### Añadir algún módulo nuevo.
En vez de hacer lo que hacía antes: 

1) Instala el módulo nuevo (con su versión).
2) Vuelve a meter TOOOODO el `pip freeze` en el documento `requirements.txt`.
3) Añade al repositorio el nuevo documento `requirements.txt`.
4) Comentas la razón del cambio.
5) Subes al servidor dichos cambios.

```
pip install new_package==1.2.3
pip freeze > requirements.txt
git add requirements.txt
git commit "Update libs"
git push
```
-----
Hacer esto ahora:

1) Instala el módulo nuevo (con su versión).
2) Añade **A MANO** al documento `requirements.txt` la librería nueva (con su versión)
3) Añade al repositorio el nuevo documento `requirements.txt`.
4) Comentas la razón del cambio.
5) Subes al servidor dichos cambios.

```
pip install new_package==1.2.3
gedit requirements.txt  # add "new_package=1.2.3" into file
git add requirements.txt
git commit "Add library: 'new_package==1.2.3'"
git push
```

Btw: sustituir "new_package" por la librería en cuestión, con su versión.

La razón de hacerlo de esta manera es que no esté en el archivo requirements todas las dependencias de cada librería. 
Por ejemplo; numpy tiene las dependencias

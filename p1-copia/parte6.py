#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 13:24:41 2019

@author: hector
"""

# %% Punto 6 : Determinación de la constante de desintegración de un radionúclido de vida corta.
"""
Resultados a tener:
- [ ] a) Efectuar un gráfico en el que se representen las cuentas registradas en ∆t (con su barra de error) frente al tiempo
- [ ] b) En la expresión (6.6) se tratan N 20, b y λ2 como parámetros desconocidos. A partir del gráfico, se efectúa una primera estimación de N20, b y λ2.
- [ ] c) Utilizar el programa de ajuste favorito (por ejemplo ROOT) para obtener los parámetros N 20, b y λ2 . En el software de los equipos PHYWE existen opciones para realizar dicho ajuste, que pueden utilizarse durante la sesión de laboratorio para realizar una primera estimación de los parámetros.
- [ ] d) A partir de λ_2, dedúzcase el valor de la vida media del estado excitado de 137Ba.
- [ ] e) Dibujar las dos contribuciones de la expresión (6.6) y su suma sobre el gráfico anterior.
- [ ] f) Se obtiene una estimación del error sistemático originado en el caso que el cambio de intervalo se hace manualmente, corrigiendo la serie de los valores temporales del ajuste de tj=(j−1)·∆t a t_j=(j−1)·∆t+(j−1)·0.1[s], donde j=1...M representa la sucesión de intervalos temporales medidos y M es el número total de intervalos, es decir, un retraso acumulado de 0.1 s en cada intervalo. Poner el error sistemático así obtenido independientemente del estadístico en los resultados del informe.
"""

"""
Tareas:
    b)  texto entero
    c)  1)  scipy.odr
        2)  sdf
        3)  dsfa
    d)
    e)
    f)
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import odr
import time
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std

# %% Leer


def leer(file_to_read, skipheader=3, delimiter=", "):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file,
                                 skip_header=skipheader,
                                 delimiter=delimiter)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def exponential(x, Amplitude, freq, HOffset=0, VOffset=0):
    return Amplitude*np.exp(freq*(x-HOffset))+VOffset


def exponential4odr2(params, x):
    return params[0]*np.exp(params[1]*(x))


def exponential4odr3(params, x):
    return params[0]*np.exp(params[1]*(x))+params[2]


def exponential4odr4(params, x):
    return params[0]*np.exp(params[1]*(x-params[3]))+params[2]


def linea(x, a, b):
    return a*x+b


# %% Workspace
shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/Esteban Rubio/"
labdir = shared + "Lab FNP/"
p1dir = labdir + "p1/"
p2dir = labdir + "p2/"
data1dir = p1dir + "datos/"
data2dir = p2dir + "datos/"
fondodir = data1dir + "fondo/"
results1dir = p1dir + "resultados y plots/decay/"

# %% leer datos
file_to_read = data1dir+"decay/decay.txt"
with open(file_to_read, "r") as opened_file:
    decay = np.genfromtxt(opened_file,
                          delimiter=",00\t",
                          skip_header=3)

x = decay[:, 0]
t = 30*decay[:, 0]
counts = decay[:, 1]
terrorx = 1*np.ones(len(decay[:, 0]))
terrory = terrorx
factor_de_escala = 1  # precisión
terrorx_grafico = factor_de_escala*terrorx
#terrorx_grafico[10]=1000
print(terrorx_grafico)

# %% 6.a - exploratory
# Efectuar un gráfico en el que se representen las cuentas registradas en ∆t (con su barra de error) frente al tiempo

fig1 = plt.figure(1, figsize=(7, 7))
plt.suptitle("Decaimiento del $^{137}$Ba en función del tiempo", fontsize=16)
plt.subplot(1, 1, 1)
plt.title("Errores aumentados en un factor 15, para visualizacion", fontsize=10)
plt.errorbar(t, counts, xerr=terrorx_grafico, fmt="b.", label="Experimental data")
plt.xlabel("Time [sec]")
plt.ylabel("Counts [#Events]")
plt.legend(loc="best")
plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(results1dir+"DECAY_exploratory.png")
plt.show(fig1)
# %% 6.b - texto
# En la expresión 6.6 se tratan N_20, b y λ_2 como parámetros desconocidos. A partir del gráfico, se efectúa una primera estimación de N_20, b y λ2.


"""
cosa de escribir
"""
# %% 6.c - ajuste
# Utilizar el programa de ajuste favorito (por ejemplo ROOT) para obtener los parámetros N_20, b y λ_2 . En el software de los equipos PHYWE existen opciones para realizar dicho ajuste, que pueden utilizarse durante la sesión de laboratorio para realizar una primera estimación de los parámetros.

exp_model = odr.Model(exponential4odr3)
mydata = odr.RealData(t, counts, sx=terrorx_grafico)
initial_guess = [max(counts), 0, 0]
myodr = odr.ODR(mydata, exp_model, initial_guess)
results = myodr.run()
results.pprint()

parameter_names = ["Amplitud",
                   "Frecuencia",
                   "Vertical Offset (Baseline)",
                   "Horizontal Offset"]

fit_parameters = np.zeros((len(initial_guess), 2))
for i in range(len(initial_guess[:])):
    fit_parameters[i, 0] = results.beta[i]
    fit_parameters[i, 1] = np.sqrt(results.cov_beta[i, i])
    print("%s:" %parameter_names[i],
          fit_parameters[i, 0], "+-", fit_parameters[i, 1])
r2 = results.sum_square_delta/results.sum_square
print("r²: ", r2)

fig2 = plt.figure(2, figsize=(10, 8))
plt.suptitle("Decaimiento del $^{137}$Ba en función del tiempo", fontsize=16)
plt.subplot(1, 1, 1)
plt.title("Errores aumentados en un factor 15, para visualizacion", fontsize=10)
plt.errorbar(t, counts, xerr=terrorx_grafico, fmt="b.", label="Experimental data", zorder=1)
labelfit = """Exponential fit: y(t) = a$\cdot e^{b \cdot t}+c$
    a = %1.0f $\pm$ %1.0f
    b = %1.5f $\pm$ %1.5f
    c = %1.0f $\pm$ %1.0f
r² = %1.5f""" % (fit_parameters[0, 0], fit_parameters[0, 1],
                 fit_parameters[1, 0], fit_parameters[1, 1],
                 fit_parameters[2, 0], fit_parameters[2, 1],
                 r2)
plt.plot(results.xplus, results.y, "k--", label=labelfit, zorder=2)
plt.plot(t, fit_parameters[2, 0]*np.ones(len(t)), "r--",
         label="Baseline: %1.0f $\pm$ %1.0f"
         % (fit_parameters[2, 0], fit_parameters[2, 1]))
plt.xlabel("Time [sec]")
plt.ylabel("Counts [#Events]")
plt.legend(loc="best")
plt.grid()
fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(results1dir+"DECAY_fitted.png")
plt.show(fig2)

# %% 6.d - vida  media
# A partir de λ_2, dedúzcase el valor de la vida media del estado excitado de ¹³⁷Ba.
vidamediaround = 0
vidamedia = np.round(-1/fit_parameters[1, 0], vidamediaround)
errorvidamedia = np.round(-vidamedia*fit_parameters[1, 1]/fit_parameters[1, 0], vidamediaround)
print("\nVida media:", int(vidamedia), "+-", int(errorvidamedia), "segundos.")

# %% 6.e - dibujar baseline y
# Dibujar las dos contribuciones de la expresión (6.6) y su suma sobre el gráfico anterior.
plt.show(fig2)

# %% 6.f - movidote de diffs de hists
# Se obtiene una estimación del error sistemático originado en el caso que el cambio de intervalo se hace manualmente, corrigiendo la serie de los valores temporales del ajuste de tj=(j−1)·∆t a t_j=(j−1)·∆t+(j−1)·0.1[s], donde j=1...M representa la sucesión de intervalos temporales medidos y M es el número total de intervalos, es decir, un retraso acumulado de 0.1 s en cada intervalo. Poner el error sistemático así obtenido independientemente del estadístico en los resultados del informe.
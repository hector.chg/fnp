#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 15 11:50:36 2019

@author: hector
"""

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import time

shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
esteban = shared + "Esteban Rubio/"
fisdir = esteban + "Lab FNP/"
p1dir = fisdir + "p1/"
datadir = p1dir + "datos/"
fondodir = datadir + "fondo/"
plomodir = datadir + "plomo/"
resultsdir = p1dir + "resultados y plots/plomo/python/"


def leer(file_to_read, skipheader=3, delim=", "):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file,
                                 skip_header=skipheader,
                                 delimiter=delim)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def gaussian(x, mean, sigma, norm=1, despl=0):
    # requires numpy
    coeff = norm/(sigma*(np.sqrt(2*np.pi)))
    exponent = -0.5*((x-mean)**2)/sigma
    fwhm = 2*np.sqrt(2*np.log(2))*sigma
    return norm*np.exp(exponent)+despl


# %% initial var declaration
data = leer(plomodir+"plomo_norm_clean.txt", 0, ", ")
NChannels, NStacks = data.shape

# rango PARA HACER EL FIT:
init = 1450
fin = 2000
N = fin-init
col_selection = 1  # A QUÉ QUIERES AJUSTAR
x = data[:, 0]
y = data[:, col_selection]
x_fit = data[init:fin, 0]
y_fit = data[init:fin, col_selection]

fig1 = plt.figure(1, figsize=(6, 3))
plt.suptitle("Initial dataviz", size=20)
plt.plot(x_fit, y_fit, label="real data")
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
plt.grid()
plt.legend("best")
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.show(fig1)

# %% cálculo
initial_guess = (1710, 3000, 3000, 0)
tic = time.time()
popt, pcov = curve_fit(gaussian, x_fit, y_fit, initial_guess)
toc = time.time()
totaltime = toc-tic
print("Time:", totaltime, "sec")


# %%var reassignation
media = popt[0]
errormedia = pcov[0, 0]
sigma = popt[1]
errorsigma = pcov[1, 1]
altura = popt[2]
erroraltura = pcov[2, 2]
integral = np.sqrt(2*np.pi)*altura*np.abs(sigma)
errorintegral = np.sqrt((2*np.pi)*(((sigma*erroraltura)**2)+((altura*errorsigma)**2)))

# %% Resultados
print("Parámetros:\n", popt, "\n")

print("Valores relevantes con sus errores:")
print(" Media:", media, " +- ", errormedia)
print(" STD:", sigma, " +- ", errorsigma)
print(" Norm. const:", altura, " +- ", erroraltura)
print(" Integral:", integral, " +- ", errorintegral)
print(" Despl. vertical (baseline):", popt[3], " +- ", pcov[3, 3], "\n")

print("Matriz de covarianzas:\n", pcov, "\n")

print("Tiempo en ajustar:\n", totaltime, "segundos\n\n")


# %% plot gaussian
y_fit_result_partial = gaussian(x_fit, *popt)

fig2 = plt.figure(2, figsize=(8, 6))
plt.suptitle("Gaussian fitting", size=20)
plt.plot(x_fit, y_fit, "b.", label="real data")
plt.plot(x_fit, y_fit_result_partial, "r-", label="real fit")
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
plt.grid()
plt.legend()
fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"AjusteGaussian.png")
plt.show(fig2)

# %% preparation for gaussian fitting in a loop
y_pred = 0.055

calibrado_grosor = np.array([[0, 0],
                             [0.032, 1120],
                             [y_pred, 1716],
                             [0.064, 2066],
                             [0.125, 3448],
                             [0.250, 7367]])

fig3 = plt.figure(3, figsize=(15, 20))
plt.suptitle("Gaussian fitting", size=20)
initial_guess = (1710, 3000, 3000, 0)
globaltime=0
for i in range(NStacks-1):

    # data to fit
    y_fit = data[init:fin, i+1]

    # processing fit
    tic = time.time()
    popt, pcov = curve_fit(gaussian, x_fit, y_fit, initial_guess)
    toc = time.time()
    totaltime = toc-tic
    globaltime += totaltime

    # y axis data for gaussian fit
    y_fit_result = gaussian(x, *popt)

    # var reassignment
    media = popt[0]
    errormedia = pcov[0, 0]
    sigma = popt[1]
    errorsigma = pcov[1, 1]
    altura = popt[2]
    erroraltura = pcov[2, 2]
    integral = np.sqrt(2*np.pi)*altura*np.abs(sigma)
    part1 = np.sqrt(2*np.pi)
    part2 = (sigma*erroraltura)**2+(altura*errorsigma)**2
    errorintegral = part1*part2

    print("Parámetros para la placa de plomo de %s gr·cm⁻²:"
          % int(calibrado_grosor[i, 1]))
    print(" Media:", media, " +- ", errormedia)
    print(" STD:", sigma, " +- ", errorsigma)
    print(" Norm. const:", altura, " +- ", erroraltura)
    print(" Integral:", integral, " +- ", errorintegral)
    print(" Despl. vertical (baseline):", popt[3], " +- ", pcov[3, 3])
    print("Matriz de covarianzas:\n", pcov)
    print("Tiempo en ajustar:\n", totaltime, "segundos\n")

    # actual plotting action
    plt.subplot(6, 1, i+1)
    plt.title("Plomo = %s gr·cm⁻²" % int(calibrado_grosor[i, 1]), fontsize=12)
    # every real data point
    plt.plot(x, data[:, i+1], "b.", label="Normalized and noiseless data")
    # fitted data for all x
    plt.plot(x, y_fit_result, "r-", label="Gaussian fit") #gaussian line

    # gaussian peak plot
    plt.plot()
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
    plt.legend()
    del(popt, pcov, tic, toc, totaltime)
print("Total fitting time: ", globaltime, "seconds.")
fig3.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"GlobalGaussianFit.png")
plt.show(fig3)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: hector
Created on Mon Oct 21 12:46:10 2019
"""

# %% Importar librerías
import numpy as np
import matplotlib.pyplot as plt

# %% Declarar espacio de trabajo

shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
esteban = shared + "Esteban Rubio/"
fisdir = esteban + "Lab FNP/"
p1dir = fisdir + "p1/"
datadir = p1dir + "datos/"
fondodir = datadir + "fondo/"
espectrosdir = datadir + "espectros 300s/"
wobackgrounddir = datadir + "espectros 300s sin fondo/"
resultsdir = p1dir + "resultados y plots/"

# %% Funciones


def leer(file_to_read, skipheader=3):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file, skip_header=skipheader)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def normcountssec(data, time=300):
    return data/time


def poner_ceros(signal):
    signal = signal[signal < 0] = 0
    return signal


# %% Importar datos
Na22 = leer(espectrosdir+"Na22 300s")
Mn54 = leer(espectrosdir+"mn54 300s")
Cs137 = leer(espectrosdir+"Cs137 300s")
Co60 = leer(espectrosdir+"co60+300s")

fondo1 = leer(fondodir+"fondo1")
fondo2 = leer(fondodir+"fondo1")
firstpass = True

arrayelementos = ["Na22", "Co60", "Mn54", "Cs137"]

# %% Meter todo en una sola matriz para facilidad con fors

if firstpass is True:
    elementos = np.column_stack((Na22, Co60[:, 1]))
    elementos = np.column_stack((elementos, Mn54[:, 1]))
    elementos = np.column_stack((elementos, Cs137[:, 1]))
    del(Na22, Mn54, Cs137, Co60)
    np.savetxt(espectrosdir+"espectros.txt", elementos,
               fmt="%0.1i", delimiter="\t", header="header\n\n")

    fondo_avg = (fondo1[:, 1] + fondo2[:, 1])/2
    fondo = np.array(list(zip(elementos[:, 0], fondo_avg)))
    del(fondo1, fondo2, fondo_avg)

    firstpass = False
    AlreadyNormalizedSignal = False
    AlreadyNormalizedNoise = False

# %% Normalizar señal y fondo
# Normalizar señal
NChannels, NElementos = elementos.shape
elementos_norm = np.zeros((NChannels, NElementos))  # inicializar variable
elementos_norm[:, 0] = elementos[:, 0]  # primera columna es channel
if AlreadyNormalizedSignal is False:
    for i in range(NElementos-1):
        elementos_norm[:, i+1] = normcountssec(elementos[:, i+1])
    elementos = elementos_norm
    del(elementos_norm)
    np.savetxt(espectrosdir+"elementos_normalizado.txt", elementos,
               fmt="%0.5f", delimiter="\t", header="header\n\n")
    AlreadyNormalizedSignal = True

# Normalizar fondo
if AlreadyNormalizedNoise is False:
    fondo[:, 1] = normcountssec(fondo[:, 1])  # normaliza por defecto a 300s
    AlreadyNormalizedNoise = True

# Flag reposition
if AlreadyNormalizedNoise is True and AlreadyNormalizedSignal is True:
    AlreadyNormalized = True
    AlreadyCleanedSignal = False

# Restar fondo normalizado a señal normalizada
elementos_fondo = np.zeros((NChannels, NElementos))
elementos_fondo[:, 0] = elementos[:, 0]  # primera columna es channel
if AlreadyCleanedSignal is False and AlreadyNormalized is True:
    for i in range(NElementos-1):
        elementos_fondo[:, i+1] = elementos[:, i+1] - fondo[:, 1]
    elementos = elementos_fondo
    del(elementos_fondo)
    AlreadyCleanedSignal = True

# %% Inital data exploration
fig1 = plt.figure(1, figsize=(10, 10))
plt.suptitle("Espectros normalizados")
for i in range(4):
    plt.subplot(3, 2, i+1)
    plt.ylabel("Count")
    plt.title(str(arrayelementos[i]))
    plt.plot(elementos[:, 0], elementos[:, i+1])
    plt.grid()
plt.subplot(3, 2, 5)
plt.title("fondo")
plt.plot(elementos[:, 0], fondo[:, 1], "k.", markersize=2)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.grid()
plt.savefig(resultsdir+"EspectrosOriginales.png")
plt.show(fig1)


fig2 = plt.figure(2, figsize=(8, 10))
plt.title("Todos los espectros. Normalizados y sin fondo")
for i in range(len(arrayelementos)):
    plt.plot(elementos[:, 0], elementos[:, i+1], label=arrayelementos[i])

leg = plt.legend(loc="best")
leg.get_frame().set_alpha(1)
plt.plot(fondo[:, 0], fondo[:, 1], "k.")
plt.xlabel("Channel")
plt.ylabel("Counts/second")
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.grid()
plt.savefig(resultsdir+"AllEspectra.png")
plt.show(fig2)


# %% volcar medidas sin fondo
Na22 = np.array(list(zip(elementos[:, 0], elementos[:, 1])))
Mn54 = np.array(list(zip(elementos[:, 0], elementos[:, 2])))
Cs137 = np.array(list(zip(elementos[:, 0], elementos[:, 3])))
Co60 = np.array(list(zip(elementos[:, 0], elementos[:, 4])))

np.savetxt(wobackgrounddir+"Na22.txt", Na22, fmt="%0.5f",
           delimiter="\t", header="header\n\n")
np.savetxt(wobackgrounddir+"Mn54.txt", Mn54, fmt="%0.5f",
           delimiter="\t", header="header\n\n")
np.savetxt(wobackgrounddir+"Cs137.txt", Cs137, fmt="%0.5f",
           delimiter="\t", header="header\n\n")
np.savetxt(wobackgrounddir+"Co60.txt", Co60, fmt="%0.5f",
           delimiter="\t", header="header\n\n")

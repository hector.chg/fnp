#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 11:17:26 2019

@author: hector
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy.signal import find_peaks, find_peaks_cwt
import peakutils
from scipy.odr import Model, RealData, ODR
import time

# %% Workspace
shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
esteban = shared + "Esteban Rubio/"
fisdir = esteban + "Lab FNP/"
p1dir = fisdir + "p1/"
datadir = p1dir + "datos/"
fondodir = datadir + "fondo/"
plomodir = datadir + "plomo/"
resultsdir = p1dir + "resultados y plots/plomo/python/"

# %% funciones


def leer(file_to_read, skipheader=3):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file, skip_header=skipheader)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def escribir(file_to_write_to, data):
    try:
        with open(file_to_write_to, "w") as opened_file:
            print(f"Purchase Amount: {data}", file=opened_file)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return "Success"


def normcountssec(data, time=300):
    return data/time


def gaussian(x, norm, mean, sigma, despl=0):
    # require numpy
    coeff = norm/(sigma*(np.sqrt(2*np.pi)))
    exponent = -0.5*((x-mean)**2)/sigma
    fwhm = 2*np.sqrt(2*np.ln(2))*sigma
    return coeff*np.exp(exponent)+despl


def expfunc(p, x):
    a, b, c, d = p
    return a*np.exp(c*(x-d))+b

#def restar_fondo(signalnoise, fondo1, fondo2):
#    fondo = (fondo2[:, 1] + fondo1[:, 1])/2
#    signal = signalnoise - fondo
#    signal[signal < 0] = 0
#
##    signal = np.array(list(zip(channel, signal)))
#
##    Na22_fondo = Na22[:, 1] - fondo[:, 1]
##    Na22_fondo[Na22_fondo < 0] = 0  # pone zeros en los numeros negativos
##    Na22_fondo = np.array(list(zip(channel, Na22_fondo)))
#    return signal
    

# %% leer
sinplomo = leer(plomodir+"cs137-6000c-52'15s.txt")
sinplomo60s = leer(plomodir+"cs137-60s.txt")  # faltaría sumar eje y

plomo0032 = leer(plomodir+"cs137-54s-0.032.txt")
plomo0064 = leer(plomodir+"cs137-57s-0.064.txt")
plomo0125 = leer(plomodir+"cs137-61s-0.125.txt")
plomo0250 = leer(plomodir+"cs137-79s-0.250.txt")
plomo1716 = leer(plomodir+"cs137-57s-1.716grcm-2.txt")
fondo1 = leer(fondodir+"fondo1")
fondo2 = leer(fondodir+"fondo1")
firstpass = True

# %% Extrapolar medida de 1716grcm2 para acabar de montar la matrix
calibrado_grosor = np.array([[0.032, 1120],
                             [0.064, 2066],
                             [0.125, 3448],
                             [0.250, 7367]])

x = calibrado_grosor[:, 0].reshape((-1, 1))
y = calibrado_grosor[:, 1]

model1 = LinearRegression()
model1.fit(x, y)
model1 = LinearRegression().fit(x, y)
r_sq1 = model1.score(x, y)
print("\ny = mx+n")
print("n = ", model1.intercept_)
print("m = ", model1.coef_)
print("r =", r_sq1)

y_pred = np.round((1716 - model1.intercept_)/(model1.coef_), 3)
y_pred = y_pred.item(0)

plomo0055 = plomo1716  # rename file with value of espesor
del(plomo1716)

calibrado_grosor = np.array([[0, 0],
                             [0.032, 1120],
                             [y_pred, 1716],
                             [0.064, 2066],
                             [0.125, 3448],
                             [0.250, 7367]])
print(calibrado_grosor)
del(x, y, model1, r_sq1)

## %%Montar dataframe de datos.
## Lo ditcheo porque es dificil llamar iterativamente
#plomo = pd.DataFrame(sinplomo[:, 1], columns=["52.15sec, 0in"])
#plomo["54sec, 0.032in"] = plomo0032[:, 1]
#plomo["57sec, 0.055in"] = plomo0055[:, 1]
#plomo["57sec, 0.064in"] = plomo0064[:, 1]
#plomo["61sec, 0.125in"] = plomo0125[:, 1]
#plomo["79sec, 0.25in"] = plomo0250[:, 1]
#plomo["60sec, sin plomo"] = sinplomo60s[:, 1]
#
#plomo = pd.DataFrame(plomo, dtype=int)  # every number integer
#plomo.describe()  # explore object

# %% Montar tan solo una matriz con toda la info para poder rular fors
if firstpass is True:
    plomo = np.column_stack((sinplomo, plomo0032[:, 1]))
    plomo = np.column_stack((plomo, plomo0055[:, 1]))
    plomo = np.column_stack((plomo, plomo0064[:, 1]))
    plomo = np.column_stack((plomo, plomo0125[:, 1]))
    plomo = np.column_stack((plomo, plomo0250[:, 1]))
    del(sinplomo, plomo0032, plomo0055, plomo0064, plomo0125, plomo0250)
    np.savetxt(plomodir+"plomo.txt", plomo, fmt='%1.1i', delimiter=", ")

    fondo_avg = (fondo1[:, 1] + fondo2[:, 1])/2
    fondo = np.array(list(zip(fondo1[:, 0], fondo_avg)))
    del(fondo1, fondo2, fondo_avg)

    firstpass = False
    AlreadyNormalizedSignal = False
    AlreadyNormalizedNoise = False
# %% Normalizar señales al tiempo

tiempos = np. array([[52.15, 54, 57, 57, 61, 79]])

# normalizar señal al tiempo
NChannels, NStacks = plomo.shape
plomo_norm = np.zeros((NChannels, NStacks))  # inicializar variable
plomo_norm[:, 0] = plomo[:, 0]  # primera columna es channel
if AlreadyNormalizedSignal is False:
    for i in range(NStacks-1):
        plomo_norm[:, i+1] = normcountssec(plomo[:, i+1], tiempos[0, i])
    plomo = plomo_norm
    del(plomo_norm)
    np.savetxt(plomodir+"plomo_normalizado.txt", plomo, fmt="%1.4f",
               delimiter=",")
    AlreadyNormalizedSignal = True

fig1 = plt.figure(1, figsize=(20, 10))
plt.suptitle("Espectros normalizados (aún con ruido)", size=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    plt.plot(plomo[:, 0], plomo[:, i+1], ".",
             label="Plomo = %s in" % str(calibrado_grosor[i, 0]))
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"EspectrosPlomoNormalizados.png")
plt.show(fig1)
del(i)


# normalizar fondo al tiempo
if AlreadyNormalizedNoise is False:
    fondo[:, 1] = normcountssec(fondo[:, 1])  # normaliza por defecto a 300s
    AlreadyNormalizedNoise = True

fig2 = plt.figure(2)
plt.title("Ruido promedio", fontsize=16)
plt.plot(fondo[:, 0], fondo[:, 1], "k.")
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
plt.grid()
plt.savefig(resultsdir+"RuidoPromedioNormalizado.png")
plt.show(fig2)

if AlreadyNormalizedNoise is True and AlreadyNormalizedSignal is True:
    AlreadyNormalized = True
    AlreadyCleanedSignal = False

# %% restar fondos normalizados a señales normalizadas
plomo_fondo = np.zeros((NChannels, NStacks))
plomo_fondo[:, 0] = plomo[:, 0]  # primera columna es channel
if AlreadyCleanedSignal is False and AlreadyNormalized is True:
    for i in range(NStacks-1):
        plomo_fondo[:, i+1] = plomo[:, i+1] - fondo[:, 1]
    plomo = plomo_fondo
    del(plomo_fondo)
    AlreadyCleanedSignal = True
del(i)

# %% plotear señal sin ruido normalizada
fig3 = plt.figure(3, figsize=(20, 10))
plt.suptitle("Espectros normalizados y sin ruido", size=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    plt.plot(plomo[:, 0], plomo[:, i+1], ".",
             label="Plomo = %s in" % str(calibrado_grosor[i, 0]))
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
fig3.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"EspectrosPlomoNormalizadosSinruido.png")
plt.show(fig3)
del(i)

# %% peakfinding scipy 1
fig4 = plt.figure(4, figsize=(20, 10))
fig4.suptitle("find_peaks", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    peaks, _ = find_peaks(plomo[:, i+1], prominence=0.2)

    plt.plot(plomo[:, i+1], "r.", peaks, plomo[peaks, 1], "bx")
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peakfinding-scipy.png")
plt.show(fig4)

# %% peakfinding peakutils
threshold = 0.18/max(plomo[:, 1])
min_distance = 3000
indexes = peakutils.indexes(plomo[:, 1],
                            thres=threshold,
                            min_dist=min_distance)
print(indexes, "\n")
print(plomo[indexes, 1])
interpolatedIndexes = peakutils.interpolate(plomo[:, 0], plomo[:, 1],
                                            ind=indexes)
print(interpolatedIndexes)

fig5 = plt.figure(5, figsize=(20, 10))
fig5.suptitle("peakutils", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    indexes = peakutils.indexes(plomo[:, i+1],
                                thres=threshold,
                                min_dist=min_distance)
    interpolatedIndexes = peakutils.interpolate(plomo[:, i], plomo[:, i+1],
                                                ind=indexes)

    plt.plot(plomo[:, i+1], "r.")
    plt.plot(indexes, plomo[indexes, 1], "bx", markersize=10)
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peakfinding-peakutils.png")
plt.show(fig5)

# %% peakfinding scipy 2
ti = time.time()

fig6 = plt.figure(6, figsize=(20, 10))
fig6.suptitle("find_peaks_cwt", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    peakind = find_peaks_cwt(plomo[:, i+1], np.arange(200, 500))
    peakind, plomo[peakind, 1]
    plt.plot(plomo[:, i+1], "r.",
             peakind, plomo[peakind, 1], "bx")
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
    print("Plomo = %s in" % str(calibrado_grosor[i, 0]), peakind)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peaks-find_peaks_cwt.png")
plt.show(fig6)

tf = time.time()
totaltime = tf-ti
print(totaltime, " sex")
del(ti, tf, totaltime)

# %% procesado para estudio de la dependencia de la señal con el tiempo
grosores = np.array([[0, 0.032, y_pred, 0.064, 0.125, 0.250]])
grosores = np.sort(grosores)
print("Grosores:\n", grosores)

tiempos = np. array([[52.15, 54, 57, 57, 61, 79]])
print("Tiempos:\n", tiempos)

# Ajuste
x = grosores.reshape((-1, 1))
y = tiempos[0, :]

model2 = LinearRegression()
model2.fit(x, y)
model2 = LinearRegression().fit(x, y)
r_sq2 = model2.score(x, y)
print("\ny = mx+n")
print("m = ", model2.coef_)
print("n = ", model2.intercept_)
print("r =", r_sq2)

x_line = np.linspace(-0.01, 0.27, 101)
y_line = model2.coef_*x_line+model2.intercept_

# data exploration
fig7 = plt.figure(7, figsize=(8, 5))
plt.title("Dependencia del tiempo que se tarda en llegar a 6000 cuentas con el grosor del plomo")
plt.scatter(grosores, tiempos)
plt.plot(x_line, y_line, "r")
fig7.tight_layout()
plt.xlabel("Grosor [in]")
plt.ylabel("Tiempo [s]")
plt.grid()
plt.savefig(resultsdir+"Grosor y tiempo.png")
plt.show(fig7)

# %% Escribir para exportado por root
for i in range(NStacks-1):
    mutevar = np.array(list(zip(plomo[:, 0], plomo[:, i+1])))

    np.savetxt(plomodir+"%s in sin fondo.txt" % (grosores[0, i]),
               mutevar,
               fmt="%0.7f",
               delimiter="\t",
               header="header\n\n")
    del(mutevar)

# %% MAGIA DE ROOT

# %% nuevos datos
# Grosores [in], Densidades [g*cm⁻²], posicion pico y error (centroide), altura pico (cuentas por segundo), sigma	
plomo_sheets_data_labels = ("Grosor [in]",
                            "Densidad superficial [gr*cm^-2]",
                            "Tiempo hasta 6000cuentas [sec]",
                            "Índice del pico [canal]",
                            "Error del índice del pico [canales]"
                            "Altura pico [cuentas/segundo]",
                            "Error de la altura del pico [cuentas/segundo]",
                            "Desviaciones std de las gaussianas [canales]",
                            "Errores de las std's [canales]")

tiempos = np.array(np.transpose(tiempos))
plomo_sheets_data = np.concatenate((calibrado_grosor, tiempos), axis=1)

peak_xposition = np.array([[1713, 8],
                           [1709, 9],
                           [1702, 8],
                           [1712, 9],
                           [1712, 9],
                           [1708, 12]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_xposition), axis=1)

peak_yposition = np.array([[0.29, 0.06],
                           [0.26, 0.06],
                           [0.26, 0.06],
                           [0.24, 0.06],
                           [0.23, 0.06],
                           [0.14, 0.05]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_yposition), axis=1)

peak_sigmas = np.array([[46, 7],
                        [50, 8],
                        [46, 8],
                        [47, 7],
                        [42, 8],
                        [45, 11]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_sigmas), axis=1)

# %% data fit - xponential

x = plomo_sheets_data[:, 1]
y = plomo_sheets_data[:, 5]
erry = plomo_sheets_data[:, 6]

# Create a model for fitting.
exp_model = Model(expfunc)

# Create a RealData object using our initiated data from above.
data = RealData(x, y, sy=erry)

# Set up ODR with the model and data.
odr = ODR(data, exp_model, beta0=[0., 0., 0., 0.])

# Run the regression.
out = odr.run()

# Use the in-built pprint method to give us results.
results_plomo = out.pprint()
np.savetxt(resultsdir+"resultadosplomo.txt", results_plomo)
'''
Beta: [ 1.01781493  0.48498006]
Beta Std Error: [ 0.00390799  0.03660941]
Beta Covariance: [[ 0.00241322 -0.01420883]
 [-0.01420883  0.21177597]]
Residual Variance: 0.00632861634898189
Inverse Condition #: 0.4195196193536024
Reason(s) for Halting:
  Sum of squares convergence
'''

dev = 0.05
x_fit = np.linspace((1-dev)*min(x), (1+dev)*max(x), num=100)
y_fit = expfunc(out.beta, x_fit)

fig9 = plt.figure(9, figsize=(7, 6))
plt.title("Decaimiento con el plomo")
plt.xlabel("Densidad superficial del plomo")
plt.ylabel("Intensidad [cuentas/segundo]")
plt.errorbar(x, y, yerr=erry, linestyle="None", marker="x")
plt.plot(x_fit, y_fit)
plt.grid()
fig9.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"DecaimientoPlomo.png")
plt.show(fig9)

# %% Punto 6, decay

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 11:17:26 2019

@author: hector
"""

# import funcionesFNP as FNP
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy.signal import find_peaks, find_peaks_cwt
from scipy.odr import Model, RealData, ODR
from scipy.optimize import curve_fit
import peakutils
import time

# %% Workspace
shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
esteban = shared + "Esteban Rubio/"
fisdir = esteban + "Lab FNP/"
p1dir = fisdir + "p1/"
datadir = p1dir + "datos/"
fondodir = datadir + "fondo/"
plomodir = datadir + "plomo/"
resultsdir = p1dir + "resultados y plots/plomo/python/"

# %% funciones


def leer(file_to_read, skipheader=3):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file, skip_header=skipheader)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def escribir(file_to_write_to, data):
    try:
        with open(file_to_write_to, "w") as opened_file:
            print(f"Purchase Amount: {data}", file=opened_file)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return "Success"


def normcountssec(data, time=300):
    return data/time


def gaussian(x, mean, sigma, norm=1, despl=0):
    # requires numpy
    coeff = norm/(sigma*(np.sqrt(2*np.pi)))
    exponent = -0.5*(((x-mean)/sigma)**2)
    fwhm = 2*np.sqrt(2*np.log(2))*sigma
    return norm*np.exp(exponent)+despl


def exponential(x, Amplitude, freq, VOffset=0):
    return Amplitude*np.exp(freq*x)+VOffset


def linea(x, a, b):
    return a*x+b


#def restar_fondo(signalnoise, fondo1, fondo2):
#    fondo = (fondo2[:, 1] + fondo1[:, 1])/2
#    signal = signalnoise - fondo
#    signal[signal < 0] = 0
#
##    signal = np.array(list(zip(channel, signal)))
#
##    Na22_fondo = Na22[:, 1] - fondo[:, 1]
##    Na22_fondo[Na22_fondo < 0] = 0  # pone zeros en los numeros negativos
##    Na22_fondo = np.array(list(zip(channel, Na22_fondo)))
#    return signal


# test own module
#sinplomo60s = FNP.leer(plomodir+"cs137-60s.txt")

# %% Punto 5: Atenuación de fotones en plomo
""" Resultados a presentar:
- [ ] a. Representar en escala semi-logarítmica I γ frente al espesor x. (Está hecho el ajuste lineal a una exponencial)
- [ ] b. El ajuste de la ecuación a estos datos proporciona el valor de μ/ρ .
- [ ] c. Comparar el valor hallado de μ/ρ con el valor tabulado para la energía de los fotones utilizados.
"""

"""
Tareas:
    a)  1) rehacer obtención de picos de gaussianas (importante la normalizacion; asegurarse que la constante de normalizacion está desacoplada de la de integración))
        2) representar frente al grosor: en densidad superficial
    b) ajuste lineal para ibtener el parametro de absorcion del plomo
    c)  1) encontrar el valor tabulado de la absorción del plomo para la energía de ese fotopico
        2) comparar y discutir
    d)
"""
# %% leer
sinplomo = leer(plomodir+"cs137-6000c-52'15s.txt")
sinplomo60s = leer(plomodir+"cs137-60s.txt")  # faltaría sumar eje y

plomo0032 = leer(plomodir+"cs137-54s-0.032.txt")
plomo0064 = leer(plomodir+"cs137-57s-0.064.txt")
plomo0125 = leer(plomodir+"cs137-61s-0.125.txt")
plomo0250 = leer(plomodir+"cs137-79s-0.250.txt")
plomo1716 = leer(plomodir+"cs137-57s-1.716grcm-2.txt")
fondo1 = leer(fondodir+"fondo1")
fondo2 = leer(fondodir+"fondo1")
firstpass = True

# %% Extrapolar medida de 1716grcm2 para acabar de montar la matrix
calibrado_grosor = np.array([[0.032, 1120],
                             [0.064, 2066],
                             [0.125, 3448],
                             [0.250, 7367]])

x = calibrado_grosor[:, 0].reshape((-1, 1))
y = calibrado_grosor[:, 1]

model1 = LinearRegression()
model1.fit(x, y)
model1 = LinearRegression().fit(x, y)
r_sq1 = model1.score(x, y)
print("\ny = mx+n")
print("n = ", model1.intercept_)
print("m = ", model1.coef_)
print("r =", r_sq1)

y_pred = np.round((1716 - model1.intercept_)/(model1.coef_), 3)
y_pred = y_pred.item(0)

plomo0055 = plomo1716  # rename file with value of espesor
del(plomo1716)

calibrado_grosor = np.array([[0, 0],
                             [0.032, 1120],
                             [y_pred, 1716],
                             [0.064, 2066],
                             [0.125, 3448],
                             [0.250, 7367]])
print(calibrado_grosor)
del(x, y, model1, r_sq1)

## %%Montar dataframe de datos.
## Lo ditcheo porque es dificil llamar iterativamente
#plomo = pd.DataFrame(sinplomo[:, 1], columns=["52.15sec, 0in"])
#plomo["54sec, 0.032in"] = plomo0032[:, 1]
#plomo["57sec, 0.055in"] = plomo0055[:, 1]
#plomo["57sec, 0.064in"] = plomo0064[:, 1]
#plomo["61sec, 0.125in"] = plomo0125[:, 1]
#plomo["79sec, 0.25in"] = plomo0250[:, 1]
#plomo["60sec, sin plomo"] = sinplomo60s[:, 1]
#
#plomo = pd.DataFrame(plomo, dtype=int)  # every number integer
#plomo.describe()  # explore object

# %% Montar tan solo una matriz con toda la info para poder rular fors
if firstpass is True:
    plomo = np.column_stack((sinplomo, plomo0032[:, 1]))
    plomo = np.column_stack((plomo, plomo0055[:, 1]))
    plomo = np.column_stack((plomo, plomo0064[:, 1]))
    plomo = np.column_stack((plomo, plomo0125[:, 1]))
    plomo = np.column_stack((plomo, plomo0250[:, 1]))
    del(sinplomo, plomo0032, plomo0055, plomo0064, plomo0125, plomo0250)

    fondo_avg = (fondo1[:, 1] + fondo2[:, 1])/2
    fondo = np.array(list(zip(fondo1[:, 0], fondo_avg)))
    del(fondo1, fondo2, fondo_avg)

    firstpass = False
    AlreadyNormalizedSignal = False
    AlreadyNormalizedNoise = False
# %% Normalizar señales al tiempo

tiempos = np. array([[52.15, 54, 57, 57, 61, 79]])

# normalizar señal al tiempo
NChannels, NStacks = plomo.shape
plomo_norm = np.zeros((NChannels, NStacks))  # inicializar variable
plomo_norm[:, 0] = plomo[:, 0]  # primera columna es channel
if AlreadyNormalizedSignal is False:
    for i in range(NStacks-1):
        plomo_norm[:, i+1] = normcountssec(plomo[:, i+1], tiempos[0, i])
    plomo = plomo_norm
    del(plomo_norm)
    np.savetxt(plomodir+"plomo_normalizado.txt", plomo, fmt="%1.4f",
               delimiter=",")
    AlreadyNormalizedSignal = True

fig1 = plt.figure(1, figsize=(20, 10))
plt.suptitle("Espectros normalizados (aún con ruido)", size=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    plt.plot(plomo[:, 0], plomo[:, i+1], ".",
             label="Plomo = %s in" % str(calibrado_grosor[i, 0]))
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"EspectrosPlomoNormalizados.png")
plt.show(fig1)

# normalizar fondo al tiempo
if AlreadyNormalizedNoise is False:
    fondo[:, 1] = normcountssec(fondo[:, 1])  # normaliza por defecto a 300s
    AlreadyNormalizedNoise = True

fig2 = plt.figure(2)
plt.title("Ruido promedio", fontsize=16)
plt.plot(fondo[:, 0], fondo[:, 1], "k.")
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
plt.grid()
plt.savefig(resultsdir+"RuidoPromedioNormalizado.png")
plt.show(fig2)

if AlreadyNormalizedNoise is True and AlreadyNormalizedSignal is True:
    AlreadyNormalized = True
    AlreadyCleanedSignal = False

# %% restar fondos normalizados a señales normalizadas
plomo_fondo = np.zeros((NChannels, NStacks))
plomo_fondo[:, 0] = plomo[:, 0]  # primera columna es channel
if AlreadyCleanedSignal is False and AlreadyNormalized is True:
    for i in range(NStacks-1):
        plomo_fondo[:, i+1] = plomo[:, i+1] - fondo[:, 1]
    plomo = plomo_fondo
    del(plomo_fondo)
    AlreadyCleanedSignal = True

# save to txt file:
np.savetxt(plomodir+"plomo_norm_clean.txt", plomo,
           fmt='%1.5f', delimiter=", ")
np.savetxt(fondodir+"avg_fondo_norm_clean.txt", fondo,
           fmt='%1.5f', delimiter=", ")

# %% plotear señal sin ruido normalizada
fig3 = plt.figure(3, figsize=(20, 10))
plt.suptitle("Espectros normalizados y sin ruido", size=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    plt.plot(plomo[:, 0], plomo[:, i+1], ".",
             label="Plomo = %s in" % str(calibrado_grosor[i, 0]))
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
fig3.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"EspectrosPlomoNormalizadosSinruido.png")
plt.show(fig3)

# %% peakfinding scipy 1
fig4 = plt.figure(4, figsize=(20, 10))
fig4.suptitle("find_peaks", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    peaks, _ = find_peaks(plomo[:, i+1], prominence=0.2)

    plt.plot(plomo[:, i+1], "r.", peaks, plomo[peaks, 1], "bx")
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peakfinding-scipy.png")
plt.show(fig4)

# %% peakfinding peakutils
threshold = 0.18/max(plomo[:, 1])
min_distance = 3000
indexes = peakutils.indexes(plomo[:, 1],
                            thres=threshold,
                            min_dist=min_distance)
print(indexes, "\n")
print(plomo[indexes, 1])
interpolatedIndexes = peakutils.interpolate(plomo[:, 0], plomo[:, 1],
                                            ind=indexes)
print(interpolatedIndexes)

fig5 = plt.figure(5, figsize=(20, 10))
fig5.suptitle("peakutils", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    indexes = peakutils.indexes(plomo[:, i+1],
                                thres=threshold,
                                min_dist=min_distance)
    interpolatedIndexes = peakutils.interpolate(plomo[:, i], plomo[:, i+1],
                                                ind=indexes)

    plt.plot(plomo[:, i+1], "r.")
    plt.plot(indexes, plomo[indexes, 1], "bx", markersize=10)
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peakfinding-peakutils.png")
plt.show(fig5)


# %% peakfinding scipy 2
fig6 = plt.figure(6, figsize=(20, 10))
fig6.suptitle("find_peaks_cwt", fontsize=25)
for i in range(NStacks-1):
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    peakind = find_peaks_cwt(plomo[:, i+1], np.arange(200, 500))
    peakind, plomo[peakind, 1]
    plt.plot(plomo[:, i+1], "r-")
    plt.plot(peakind, plomo[peakind, 1], "bx", markersize=10)
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()
    print("Plomo = %s in" % str(calibrado_grosor[i, 0]), peakind)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"find_peaks_cwt.png")
plt.show(fig6)

init = 1450
fin = 2000
fig7 = plt.figure(7, figsize=(20, 10))
fig7.suptitle("picos, find_peaks_cwt", fontsize=25)
globaltime = 0
for i in range(NStacks-1):
    # peaks
    tic = time.time()
    peakind = find_peaks_cwt(plomo[init:fin, i+1], np.arange(100, 300))
    toc = time.time()
    totaltime = toc-tic
    globaltime += totaltime
    print("Plomo = %s in" % str(calibrado_grosor[i, 0]))
    print("Time: ", totaltime, " sec.")
    print("Peak x position: ", (init+peakind), "\n")

    # plot
    plt.subplot(3, 2, i+1)
    plt.title("Plomo = %s in" % str(calibrado_grosor[i, 0]), fontsize=16)
    plt.plot(plomo[init:fin, i+1], "r-")
    plt.plot(peakind, plomo[peakind, 1], "bx", markersize=10)
    plt.legend(["Signal", "Peaks"])
    plt.xlabel("Channel")
    plt.ylabel("Counts/second")
    plt.grid()

    # gauss

print("Total peak process time: ", globaltime, "sec")
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"peaks-find_peaks_cwt.png")
plt.show(fig7)
del(tic, toc, totaltime, i)


# %% procesado para estudio de la dependencia de la señal con el tiempo
grosores = np.array([[0, 0.032, y_pred, 0.064, 0.125, 0.250]])
grosores = np.sort(grosores)
print("Grosores:\n", grosores)

tiempos = np. array([[52.15, 54, 57, 57, 61, 79]])
print("Tiempos:\n", tiempos)

# Ajuste
x = grosores.reshape((-1, 1))
y = tiempos[0, :]

model2 = LinearRegression()
model2.fit(x, y)
model2 = LinearRegression().fit(x, y)
r_sq2 = model2.score(x, y)
print("\ny = mx+n")
print("m = ", model2.coef_)
print("n = ", model2.intercept_)
print("r =", r_sq2)

x_line = np.linspace(-0.01, 0.27, 101)
y_line = model2.coef_*x_line+model2.intercept_

# data exploration
fig7 = plt.figure(7, figsize=(8, 5))
plt.title("Dependencia del tiempo que se tarda en llegar a 6000 cuentas con el grosor del plomo")
plt.scatter(grosores, tiempos)
plt.plot(x_line, y_line, "r")
fig7.tight_layout()
plt.xlabel("Grosor [in]")
plt.ylabel("Tiempo [s]")
plt.grid()
plt.savefig(resultsdir+"Grosor y tiempo.png")
plt.show(fig7)

# %% Escribir para exportado por root
for i in range(NStacks-1):
    mutevar = np.array(list(zip(plomo[:, 0], plomo[:, i+1])))

    np.savetxt(plomodir+"%s in sin fondo.txt" % (grosores[0, i]),
               mutevar,
               fmt="%0.7f",
               delimiter="\t",
               header="header\n\n")
    del(mutevar)
del(i)

# %% magia de root QUE AHORA ESTÁ EN PYTHON MUAHAHAHAHAHA
# rango PARA HACER EL FIT:
init = 1450
fin = 3500
N = fin-init
col_selection = 1  # A QUÉ QUIERES AJUSTAR
x = plomo[:, 0]
y = plomo[:, col_selection]
x_fit = plomo[init:fin, 0]
y_fit = plomo[init:fin, col_selection]

fig8 = plt.figure(8, figsize=(15, 20))
plt.suptitle("Gaussian fitting", size=20)
initial_guess = (1710, 30, 3000, 0)
globaltime = 0
picos_gaussianas = np.zeros((NStacks-1, 10))
for i in range(NStacks-1):
    # data to fit
    y_fit = plomo[init:fin, i+1]

    # processing fit
    tic = time.time()
    popt, pcov = curve_fit(gaussian, x_fit, y_fit, initial_guess)
    toc = time.time()
    totaltime = toc-tic
    globaltime += totaltime

    # y axis data for gaussian fit
    y_fit_result = gaussian(x, *popt)

    # var reassignment
    media = popt[0]
    errormedia = np.sqrt(pcov[0, 0])
    sigma = popt[1]
    errorsigma = np.sqrt(pcov[1, 1])
    altura = popt[2]
    erroraltura = np.sqrt(pcov[2, 2])
    integral = np.sqrt(2*np.pi)*altura*np.abs(sigma)
    part1 = np.sqrt(2*np.pi)
    part2 = (sigma*erroraltura)**2+(altura*errorsigma)**2
    errorintegral = np.sqrt(part1*part2)
    del(part1, part2)
    picos_gaussianas[i, 0] = calibrado_grosor[i, 1]  # densidades
    picos_gaussianas[i, 1] = media  # posicion x picos
    picos_gaussianas[i, 2] = errormedia  # error posicion x picos
    picos_gaussianas[i, 3] = altura  # altura picos
    picos_gaussianas[i, 4] = erroraltura  # error altura
    picos_gaussianas[i, 5] = integral  # altura picos
    picos_gaussianas[i, 6] = errorintegral  # error altura
    picos_gaussianas[i, 7] = sum(plomo[:, i+1])  # cuentabruta
    picos_gaussianas[i, 8] = picos_gaussianas[i, 5]/picos_gaussianas[i, 7]  # ratio cuenta neta/bruta
    picos_gaussianas[i, 9] = picos_gaussianas[i, 6]/picos_gaussianas[i, 7]  # error ratio

    print("Parámetros para la placa de plomo de %s gr·cm⁻²:"
          % int(calibrado_grosor[i, 1]))
    print(" Media:", media, " +- ", errormedia)
    print(" STD:", sigma, " +- ", errorsigma)
    print(" Norm. const:", altura, " +- ", erroraltura)
    print(" Integral:", integral, " +- ", errorintegral)
    print(" Despl. vertical (baseline):", popt[3], " +- ", pcov[3, 3])
    print("Matriz de covarianzas:\n", pcov)
    print("Tiempo en ajustar:\n", totaltime, "segundos\n")

    # actual plotting action
    plt.subplot(6, 1, i+1)
    plt.title("Placa de plomo de %s gr·cm⁻²" % int(calibrado_grosor[i, 1]), fontsize=12)
    # every real data point
    plt.plot(x, plomo[:, i+1], "b.", label="Normalized and noiseless data")
    # fitted data for all x
    plt.plot(x, y_fit_result, "r-", label=
"""Gaussian fit for %1.0i gr·cm⁻²:
 $\mu$=%1.1f$\pm$%1.1f
 $\sigma$=%1.0f$\pm$%1.0f
 Altura=%1.3f$\pm$%1.3f
 Cuenta neta=%1.2f$\pm$%1.1f
 Cuenta bruta=%1.0f
 Neta/Bruta=%1.3f$\pm$%1.3f"""
 % (calibrado_grosor[i, 1],
    media, errormedia,
    sigma, errorsigma,
    altura, erroraltura,
    integral, errorintegral,
    picos_gaussianas[i, 7],
    picos_gaussianas[i, 8], picos_gaussianas[i, 9]))  # gaussian line

    # gaussian peak plot
    plt.plot()
    plt.xlabel("Channel")
    plt.ylabel("Counts/sec")
    plt.grid()
    plt.legend()
    del(popt, pcov, tic, toc, totaltime)
print("Total fitting time: ", globaltime, "seconds.")
fig8.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"GlobalGaussianFit.png")
plt.show(fig8)

plt.plot(picos_gaussianas[:, 0], picos_gaussianas[:, 8])

# %% alturas en funcion de gaussianas
# fit

x = picos_gaussianas[:, 0]
y = picos_gaussianas[:, 3]
x_log = np.log(x[1:len(x)])
N = 100
x_fit_lin = np.linspace(min(x), max(x), N)
x_fit_log = np.linspace(min(x_log), max(x_log), N)

initial_exp_guess = (0.3, -0.0001)
initial_lin_guess = (0.3, -0.0001)
# initial_params = 
popt_lin, pcov_lin = curve_fit(exponential, x, y, initial_exp_guess)
a_lin = popt_lin[0]
b_lin = popt_lin[1]
erra_lin = np.sqrt(pcov_lin[0, 0])
errb_lin = np.sqrt(pcov_lin[1, 1])
print("Ajuste exponencial en el eje lineal:\n Parámetros\n", popt_lin)
print("Matriz de covarianzas\n", pcov_lin)
print("Amplitud = ", a_lin, "+-", erra_lin)
print("Decay = ", b_lin, "+-", errb_lin, "\n")
popt_log, pcov_log = curve_fit(linea, x_log, y[1:len(y)], initial_lin_guess)
a_log = popt_log[0]
b_log = popt_log[1]
erra_log = np.sqrt(pcov_log[0, 0])
errb_log = np.sqrt(pcov_log[1, 1])
print("Ajuste lineal en el eje logarítmico:\n Parámetros\n", popt_log)
print("Matriz de covarianzas\n", pcov_log)
print("Pendiente = ", a_log, "+-", erra_log)
print("Ordenada = ", b_log, "+-", errb_log, "\n\n")

fig9 = plt.figure(9, figsize=(12, 12))
plt.suptitle("Dependency between the intensity of the photopeak and the thickness of the Pb sheet", size=14)
plt.subplot(2, 1, 1)

# lin-lin
plt.title("Linear axis; Exponential fit")
plt.plot(x, y, "b.", label="Observed experimental data")
plt.plot(x_fit_lin, exponential(x_fit_lin, popt_lin[0], popt_lin[1]),
         "r",
         label="Exponential fit:\na=%1.3f +- %1.3f \nb=%1.6f+-%1.6f" % (a_lin,  erra_lin, b_lin, errb_lin))
plt.xlabel("Superficial density of Pb sheet [g·cm⁻²]")
plt.ylabel("Counts/sec as a function of density")
plt.legend()
plt.grid()

# log-lin
plt.subplot(2, 1, 2)
plt.title("Logarithmic axis; Linear fit")
plt.plot(x_log, y[1:len(y)], "b.", label="log-masked experimental data")
plt.plot(x_fit_log, linea(x_fit_log, popt_log[0], popt_log[1]),
         "r",
         label="Linear fit:\na=%1.3f +- %1.3f \nb=%1.2f+-%1.2f" % (a_log,  erra_log, b_log, errb_log))

plt.xlabel("Superficial density of Pb sheet [g·cm⁻²]")
plt.ylabel("Counts per second for each qu superficial density")
plt.legend()
plt.grid()

fig9.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"density and intensity.png")
plt.show(fig9)

# %% con ro por x
ro_x = np.zeros((NStacks-1, 1))
in2cm = 2.54
ro_x[:, 0] = in2cm*calibrado_grosor[:, 0]*calibrado_grosor[:, 1]
plt.plot(ro_x, picos_gaussianas[:, 8], "b.")

# %% plot 2
fig10 = plt.figure(10, figsize=(12, 12))
plt.suptitle("Dependency between the intensity of the photopeak and the thickness of the Pb sheet", size=14)
plt.subplot(2, 1, 1)

# lin-lin
plt.title("Linear axis; Exponential fit")
plt.plot(x, y, "b.", label="Observed experimental data")
plt.plot(x_fit_lin, exponential(x_fit_lin, popt_lin[0], popt_lin[1]),
         "r",
         label="Exponential fit:\na=%1.3f +- %1.3f \nb=%1.6f+-%1.6f"
         % (a_lin,  erra_lin, b_lin, errb_lin))
plt.xlabel("Superficial density of Pb sheet [g·cm⁻²]")
plt.ylabel("Counts/sec as a function of density")
plt.legend()
plt.grid()

# log-lin
plt.subplot(2, 1, 2)
plt.title("Logarithmic axis; Linear fit")
plt.plot(x_log, y[1:len(y)], "b.", label="log-masked experimental data")
plt.plot(x_fit_log, linea(x_fit_log, popt_log[0], popt_log[1]),
         "r",
         label="Linear fit:\na=%1.3f +- %1.3f \nb=%1.2f+-%1.2f" % (a_log,  erra_log, b_log, errb_log))

plt.xlabel("Superficial density of Pb sheet [g·cm⁻²]")
plt.ylabel("Counts per second for each qu superficial density")
plt.legend()
plt.grid()

fig9.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"density and intensity 2.png")
plt.show(fig10)

# %% nuevos datos (de root)
# Grosores [in], Densidades [g*cm⁻²], posicion pico y error (centroide), altura pico (cuentas por segundo), sigma	
plomo_sheets_ROOTdata_labels = ("Grosor [in]",
                                "Densidad superficial [gr*cm^-2]",
                                "Tiempo hasta 6000cuentas [sec]",
                                "Índice del pico [canal]",
                                "Error del índice del pico [canales]"
                                "Altura pico [cuentas/segundo]",
                                "Error de la altura del pico [cuentas/segundo]",
                                "Desviaciones std de las gaussianas [canales]",
                                "Errores de las std's [canales]")

tiempos = np.array(np.transpose(tiempos))
plomo_sheets_data = np.concatenate((calibrado_grosor, tiempos), axis=1)

peak_xposition = np.array([[1713, 8],
                           [1709, 9],
                           [1702, 8],
                           [1712, 9],
                           [1712, 9],
                           [1708, 12]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_xposition), axis=1)

peak_yposition = np.array([[0.29, 0.06],
                           [0.26, 0.06],
                           [0.26, 0.06],
                           [0.24, 0.06],
                           [0.23, 0.06],
                           [0.14, 0.05]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_yposition), axis=1)

peak_sigmas = np.array([[46, 7],
                        [50, 8],
                        [46, 8],
                        [47, 7],
                        [42, 8],
                        [45, 11]])
plomo_sheets_data = np.concatenate((plomo_sheets_data, peak_sigmas), axis=1)

# %% data fit - exponential - con error

x = plomo_sheets_data[:, 1]
y = plomo_sheets_data[:, 5]
erry = plomo_sheets_data[:, 6]

exp_model = Model(exponential)  # Create a model for fitting.
data = RealData(x, y, sy=erry)  # Create a RealData object using our initiated data from above.
out = ODR(data, exp_model, beta0=[0.3, -0.0001, 0]).run()  # Set up ODR with the model and data and run the regression
results_plomo = out.pprint()  # Use the in-built pprint method to give us results.
np.savetxt(resultsdir+"resultadosplomo.txt", results_plomo)  # save results to file (TO.DO)
'''

'''

dev = 0.05
x_fit = np.linspace((1-dev)*min(x), (1+dev)*max(x), num=100)
y_fit = expfunc(out.beta, x_fit)

fig9 = plt.figure(9, figsize=(7, 6))
plt.title("Decaimiento con el plomo")
plt.xlabel("Densidad superficial del plomo")
plt.ylabel("Intensidad [cuentas/segundo]")
plt.errorbar(x, y, yerr=erry, linestyle="None", marker="x")
plt.plot(x_fit, y_fit)
plt.grid()
fig9.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"DecaimientoPlomo.png")
plt.show(fig9)

# LAB FNP - PRÁCTICA 1
## Punto 1: Calibración energética del sistema de detección
- [x] a. Estudiar el esquema de desintegración y los espectros del 137Cs y del 22Na.
- [x] b. Para cada uno de los fotopicos, hallar el canal del centroide y la FWHM (en número de canales), utilizando las funciones de la MCA.
- [x] c. Efectuar una representación gráfica de la curva de calibrado parecida a la de la Figura 1.5, es decir, la energía tabulada de los fotopicos frente al valor medido de su centroide en número de canales.
- [x] d. Usar el procedimiento de calibrado de tres puntos del MCA para calibrar el sistema de detección utilizando los picos de 662 keV (137Cs), 511 keV y 1275 keV (22Na). Este es un calibrado aproximado, que usaremos en el desarrollo de la práctica (online), y será sustituido por un calibrado preciso obtenido después del desarrollo de la práctica para el análisis y presentación de resultados.
- [x] e. Representar la recta que resulta sobre la gráfica de calibrado.
- [x] f. Representar la parábola que resulta sobre la gráfica de calibrado. Comprobar si la respuesta del equipo es o no lineal.
	
	
## Punto 2: Espectros gamma de fuentes radiactivas
- [x] a. Representar y estudiar los esquemas de desintegración de los radionúclidos usados.
- [x] b. Para cada uno de los fotopicos de cada espectro, hallar el canal del centroide, la FWHM (en número de canales) y la energía del centroide que proporciona el calibrado del Experimento 1.
- [x] c. Comprobar si la energía de los centroides es consistente con los valores tabulados para cada radionúclido.
- [x] d. Actualizar el gráfico de calibrado con los nuevos picos hallados en este experimento, comprobando la compatibilidad con los valores tabulados
- [x] e. Extraer las conclusiones oportunas respecto a la linealidad del sistema de detección.
	
## Punto 3: Estudio de la dispersión Compton
- [x] a. Calcular el valor esperado de energía del borde Compton con la ecuación (3.2) para los fotopicos de los espectros anteriores. ESTEBAN
- [x] b. Determinar la energía del borde Compton para cada uno de los fotopicos a partir de los espectros medidos, teniendo en cuenta que dicho borde se halla entre el continuo Compton y el fotopico a una energía que corresponde, aproximadamente, a la mitad en el cambio de tasa de cuentas (ver Figuras 1.4 y 3.1). ESTEBAN
- [x] c. Determinar la masa del electrón a partir de las energías del borde Compton de todos los fotopicos. Para ello, intentar ajustar el espectro en la región del borde Compton utilizando una función paramétrica que permita estimar el punto medio y la anchura del borde (por ejemplo, una función de tipo Saxon-Woods, cuyo template ROOT de ajuste se proporciona en el material de laboratorio). ESTEBAN
- [x] d. Calcular la energía del pico de backscattering con la ecuación (3.3) para los fotones de los espectros anteriores. 
- [x] d2. Determinar la energía del pico de backscattering para cada uno de los espectros (véase Figuras 1.4 y 3.1). ESTEBAN
- [x] e. Determinar la masa del electrón a partir de las energías de los picos de backscattering y comparar los resultados con el punto c. ESTEBAN
	
## Punto 4: Pico de aniquilación, picos de escape simple y doble, pico suma
- [ ] Identificar los picos de aniquilación - (escribir texto)
- [ ] picos de escape simple - 
- [ ] picos de escape doble  - 
- [ ] comprobar el acuerdo de los valores hallados con los valores de energía esperados - 

## Punto 5: Atenuación de fotones en plomo
- [x] a. Representar en escala semi-logarítmica I γ frente al espesor x. (Está hecho el ajuste lineal a una exponencial) FALTA BARRA DE ERROR
- [x] b. El ajuste de la ecuación a estos datos proporciona el valor de μ/ρ .
- [x] c. Comparar el valor hallado de μ/ρ con el valor tabulado para la energía de los fotones utilizados. FALTA
- [x] 1) pillar las intensidades de los picos, y su posicion, de la variable plomo
- [x] 2) plotear log-lin intensidad respecto a grosor de plomo
- [x] 3) sacar parametros de ajuste
- [ ] 4) discutir FALTA
	
## Punto 6: Determinación de la constante de desintegración de un radionúclido de vida corta.
- [x] a. Efectuar un gráfico en el que se representen las cuentas registradas en ∆t (con su barra de error) frente al tiempo - HECTOR
- [ ] b. En la expresión (6.6) se tratan N 20 , b y λ 2 como parámetros - HECTOR desconocidos. A partir del gráfico, se efectúa una primera estimación de N 20 , b y λ 2 . - HECTOR
- [x] c. Utilizar el programa de ajuste favorito (por ejemplo ROOT) para obtener los parámetros N 20 , b y λ 2 . En el software de los equipos PHYWE existen opciones para realizar dicho ajuste, que pueden utilizarse durante la sesión de laboratorio para realizar una primera estimación de los parámetros. - HECTOR
- [x] d. A partir de λ^2 , dedúzcase el valor de la vida media del estado excitado de 137 Ba. - HECTOR
- [ ] e. Dibujar las dos contribuciones de la expresión (6.6) y su suma sobre el gráfico anterior. - HECTOR
- [ ] f. Se obtiene una estimación del error sistemático originado en el caso que el cambio de intervalo se hace manualmente, corrigiendo la serie de los valores temporales del ajuste de tj=(j−1)·∆t a t_j=(j−1)·∆t+(j−1)·0.1[s], donde j=1...M representa la sucesión de intervalos temporales medidos y M es el número total de intervalos, es decir, un retraso acumulado de 0.1 s en cada intervalo. Poner el error sistemático así obtenido independientemente del estadístico en los resultados del informe. - HECTOR
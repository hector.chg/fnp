#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 19:11:55 2019

@author: hector
"""
# %%

from scipy.odr import ODR, Model, RealData
import numpy as np
import matplotlib.pyplot as plt


def func(beta, x):
    return beta[0]+beta[1]*x+beta[2]*x**3


# generate fake data
x_fake = np.linspace(-3, 2, 100)
objective_params = [-2.3, 7.0, -4.0]
y_fake = func(objective_params, x_fake)

data = RealData(x_fake, y_fake, 0.3, 0.1)
model = Model(func)

odr = ODR(data, model, [1, 0, 0])
odr.set_job(fit_type=2)
output = odr.run()


x_fit = np.linspace(-3, 2, 50)
yn = func(x_fit, output.beta)

fig1 = plt.figure(1, figsize=(10, 10))
plt.suptitle("Test title", fontsize=12)
plt.plot(x_fake, y_fake,'ro')
plt.plot(x_fit,yn,'k-',label='leastsq')

odr.set_job(fit_type=0)
output = odr.run()
yn = func(output.beta, x_fit)
plot(x_fit,yn,'g-',label='odr')
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
legend(loc="best")
plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig("testfit with weights.png")
plt.show(fig1)
PROCEDIMIENTO:
1) primero leemos todos los datos de los espectros de 137Cs tomados para cada plaa de plomo
2) despues obtenemos el valor del grosor de la placa que falta a través de un ajuste lineal, asumiendo que la relacion entre densidad superficial y el grosor es lineal
3) despues normalizamos la variable de señal al tiempo de medida
4) despues normalizamos la variable de ruido al tiempo de medida
5) restamos la variable de ruido ya normalizada a la variable de señal ya normalizada
	NOMBRE DE GRAFICO: "EspectrosPlomoNormalizadosSinRuido.png"
6) buscamos picos infructosamente por tres metodos
	MéTODOS:
		1) scipy.signal.find_peaks MALO
		2) peakutils.indexes, peakutils.interpolate REGULAR
		3) scipy.signal.find_peaks_cwt (convolucion wavelet) BUENO
7) nos damos cuenta de que la dependencia del grosor de las placas de plomo con el tiempo que se tarda en llegar a un numero de cuentas fijado es lineal
	NOMBRE DE GRÁFICO: "Grosor y tiempo.png"
8) ajustamos una gaussiana a cada fotopico, asumiendo que no hay fondo lineal pues se lo hemos restado
	VALORES (NOMBRE DEL FICHERO DE TXT CON LA INFO): "resultados_gauss_plomo.txt"
	NOMBRE DE GRÁFICO: "GlobalGaussianFit.png"
9) ploteamos la altura de cada gaussiana como función de la densidad superficial de la placa de plomo correspondiente, tano en un diagrama lineal como en uno con ejes logaritmicos
10) ajustamos dichos plots a una exponencial y=a*e(b*x) y a una recta y=a*x+b respectivamente para obtener el efecto de decay del plomo en nuestra muestra de cs137
	NOMBRE GRÁFICO: "density and intensity.png"

Conclusiones:
1) Advertimos que el tercer punto de nuestro ajuste está por encima de lo esperado por tres métodos diferentes:
	1) cocientes de conteos netos y brutos
	2) altura de pico de gaussiana respecto a grosor lineal
	3) altura de pico de gaussiana respecto a grosor logarítmico
Este punto es el correspondiente al de la placa cuyo valor tabulado de la densidad superficial es de 1716 g·cm⁻², que es la única fuera del juego de 4 placas de la caja (las que tienen carcasa). 
Podemos concluir que o el valor real de la densidad superficial es menor al que pone, (más cercano a ~1100 g·cm⁻²), o por otra parte hay un error sistemático en las otras 4 placas. 
Desestimamos ésta útlima hipótesis porque la medida sin plomo quedaría lejos de los valores corregidos. Además, al principio estábamos representando la altura del fotopico respecto al grosor de la placa en pulgadas, y como la del valor de 1716 gr/cm² no tiene grosor, tuvimos que extrapolar la medida mediante un ajuste OLS. Éste valor nos dio 0.055 pulgadas, pero fue suponiendo que el valor de 1716 era correcto
Con un segundo approach, advertimos que el tiempo en llegar a 6000 cuentas de la placa de 0.032in (valor tabulado de densidad superficial de 1120g/cm²) era de 57 segundos; el mismo tiempo que tardó en llegar a 6000 cuentas la placa problema.
Ésto revalida nuestra hipótesis de que la densidad superficial de nuestra placa problema es en realidad menor.

2) Para medir, fijamos el numero de cuentas totales a 6000 y registramos cuanto tiempo tardábamos en llegar a dicha cuenta. Al representar el tiempo tardado en completar las 6000 cuentas en función del grosor de cada placa de plomo advertimos que esta relación es lineal.	

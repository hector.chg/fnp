def leer(file_to_read, skipheader=3):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file, skip_header=skipheader)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def escribir(file_to_write_to, data):
    try:
        with open(file_to_write_to, "w") as opened_file:
            print(f"Purchase Amount: {data}", file=opened_file)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return "Success"


def normcountssec(data, time=300):
    return data/time


def gaussian(x, norm, mean, sigma, despl=0):
    # require numpy
    coeff = norm/(sigma*(np.sqrt(2*np.pi)))
    exponent = -0.5*((x-mean)**2)/sigma
    fwhm = 2*np.sqrt(2*np.ln(2))*sigma
    return coeff*np.exp(exponent)+despl


def expfunc(p, x):
    a, b, c, d = p
    return a*np.exp(c*(x-d))+b

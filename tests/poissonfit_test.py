#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 14:12:56 2019

@author: hector
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.special import factorial
from scipy import odr


# %% funciones
def poisson(k, lamb):
    """poisson pdf, parameter lamb is the fit parameter"""
    return (lamb**k/factorial(k)) * np.exp(-lamb)


def negLogLikelihood(params, data):
    """ the negative log-Likelohood-Function"""
    lnl = - np.sum(np.log(poisson(data, params[0])))
    return lnl


def poisson4odr1(param, k):
    lamb = param
    return (lamb**k/factorial(k)) * np.exp(-lamb)


# get poisson deviated random numbers
data_fake = np.random.poisson(2, 1000)

data = np.array([[0, 17],
                 [1, 27],
                 [2, 34],
                 [3, 33],
                 [4, 20],
                 [5, 11],
                 [6, 5],
                 [7, 2],
                 [8, 1],
                 [9, 0],
                 [10, 0],
                 [11, 0],
                 [12, 0],
                 [13, 0]])

# minimize the negative log-Likelihood

result_fake = minimize(negLogLikelihood,  # function to minimize
                       x0=np.ones(1),     # start value
                       args=(data_fake,), # additional arguments for function
                       method='Powell',   # minimization method, see docs
                       )

result = minimize(negLogLikelihood,  # function to minimize
                  x0=np.ones(1),     # start value
                  args=(data),      # additional arguments for function
                  method='Powell',   # minimization method, see docs
                  )
# result is a scipy optimize result object, the fit parameters
# are stored in result.x
print(result)

# plot poisson-deviation with fitted parameter
x_plot = np.linspace(min(data[:, 0]), max(data[:, 0]), 100)

plt.plot(x_plot, poisson(x_plot, result.x), 'r-')
plt.plot(x_plot, 100*poisson(x_plot, 3), "k-")
plt.plot(data[:, 0], data[:, 1], "-")
plt.plot(data[:, 0], data[:, 1], "x")
plt.show()

# %% ajuste poisson
model = odr.Model(poisson4odr1)
mydata = odr.RealData(data[:, 0], data[:, 1])
initial_guess = 10*np.ones((1))
myodr = odr.ODR(mydata, model, initial_guess)
results = myodr.run()
results.pprint()

parameter_name = ["mu"]
fit_parameters = np.zeros((1, 2))
fit_parameters[0, 0] = results.beta[0]
fit_parameters[0, 1] = np.sqrt(results.cov_beta[0, 0])
print("%s:" % parameter_name[0],
      fit_parameters[0, 0], "+-", fit_parameters[0, 1],
      "")
r2 = results.sum_square_delta/results.sum_square
print("r²: ", r2, "\n")


fig1 = plt.figure(1, figsize=(7, 7))
plt.suptitle("Frecuencia relativa de la variable discreta de fondo", fontsize=16)
plt.subplot(1, 1, 1)
plt.title("", fontsize=10)

plt.plot(x_plot, poisson(x_plot, result.x), 'r-')

plt.plot(x_plot, poisson(x_plot, fit_parameters[0, 0]),'b-',
         label="odr fit")

plt.plot(x_plot, 100*poisson(x_plot, 3), "k--", label="bruteforce")

plt.plot(data[:, 0], data[:, 1], "g-")
plt.plot(data[:, 0], data[:, 1], "gx", label="experimental observations")

plt.xlabel("Number of counts")
plt.ylabel("Relative frequency")
plt.legend(loc="best")
plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(figdir+"poissonian_noise.png")
plt.show(fig1)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 11:21:28 2019

@author: hector
"""

# % librerías

import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn import metrics
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# % Declarar espacio de trabajo
shared = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
esteban = shared + "Esteban Rubio/"
fisdir = esteban + "Lab FNP/"
p1dir = fisdir + "p1/"
datadir = p1dir + "datos/"
fondodir = datadir + "fondo/"
plomodir = datadir + "plomo/"
resultsdir = p1dir + "resultados y plots/plomo/python/"


def leer(file_to_read, skipheader=3, delimiter=", "):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file, skip_header=skipheader)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


# % Importar datos

data = leer(plomodir+"plomo_norm_clean.txt", 0, ", ")


# % Data exploration
fig1 = plt.figure(1, figsize=(20, 10))
plt.suptitle("test", size=25)
plt.scatter(data[:, 0], data[:, 1])
plt.xlabel("Channel")
plt.ylabel("Counts/sec")
plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(resultsdir+"EspectrosPlomoNormalizados.png")
plt.show(fig1)

# %% Ajuste a una recta


def recta(x, a, b):
    return a*x + b

# %% Fit parameters


x = data[:, 0]
y = data[:, 1]

fitParams, fitCovariances = curve_fit(recta, x, y)

a = fitParams[0]
b = fitParams[1]

sigma = [fitCovariances[0, 0], fitCovariances[1, 1]]

error_a = sigma[0]
error_b = sigma[1]

print("\n")
print("a = ", a, " +- ", error_a)
print("b = ", b, " +- ", error_b)

print("\n", fitCovariances)

# %% Plotting

start_x = np.min(x)
end_x = np.max(x)

plt.xlabel("eje x []", fontsize=16)
plt.ylabel("eje y []", fontsize=16)
plt.title("Título", fontsize=20)
plt.grid()
plt.box()
plt.xlim(start_x - 0.025*start_x, end_x + 0.025*end_x)

# plot the data as red circles with errorbars in the vertical direction
plt.errorbar(x, y, fmt='r.', yerr=0.2)
# now plot the best fit curve and also +- 3 sigma curves
# the square root of the diagonal covariance matrix element
# is the uncertianty on the corresponding fit parameter.
sigma = [fitCovariances[0, 0], fitCovariances[1, 1]]

plt.tight_layout()
plt.plot(x, recta(x, fitParams[0] + sigma[0], fitParams[1] - sigma[1]),
         x, recta(x, fitParams[0] - sigma[0], fitParams[1] + sigma[1]),
         x, recta(x, fitParams[0], fitParams[1]))

plt.savefig("ajusterecta", dpi=100)

# %% Using sklearn

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

regressor = LinearRegression(
regressor.fit(x_train, y_train)  # training the algorithm

#To retrieve the intercept:
regressor.intercept

#For retrieving the slope:
print(regressor.coef())



y_pred = regressor.predict(X_test)

plt.scatter(X_test, y_test,  color='gray')
plt.plot(X_test, y_pred, color='red', linewidth=2)
plt.show()
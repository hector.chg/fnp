# Práctica 2 - Estadística de recuento y espectrometría β con un detector Geiger-Mueller

---
## 1 - Determinación de la curva plateau del Geiger-Mueller

### Objetivo:

- Obtener la curva plateau del Geiger-Mueller (GM), deducir su pendiente para comprobar su estado de funcionamiento y obtener el potencial óptimo de trabajo.

### Resultados:

- [x] a) Se calcula la tasa de cuentas, `g=G/t_G` en c/min y su desviación estándar `σ_g`.
- [x] b) Se dibuja la curva plateau representando g frente a V, indicando en ella el codo y la región
de descarga continua.
- [x] c) El potencial de trabajo se elige en torno al 25% del plateau comenzando por el codo. Según las especificaciones del fabricante, éste estará en torno a 450 V.
- [x] d) Para evaluar el estado del contador Geiger-Mueller se determina la pendiente del plateau. Para ello, se dibuja una línea recta que ajuste visualmente la zona del plateau; se eligen dos puntos sobre la recta y a partir de ellos se calcula la pendiente relativa (pendiente por 100 V) definida como:

`((g2-g1)/(v2-v1))(100V/g1)% 	(FÓRMULA 1)`

- [x] e) Si el contador se halla en buen estado, la pendiente ha de ser inferior al 10%. La utilización prolongada del detector acorta la anchura del plateau y hace crecer su pendiente, fundamentalmente debido a fallos en el proceso de extinción de la avalancha  (“quenching”). Realizar también un ajuste de la línea recta de los puntos seleccionados del plateau y comparar la pendiente relativa así obtenida con la del caso anterior.

## 2 - Fondo del detector

### Objetivos:

- Determinar el fondo del detector, estudiar la distribución de Poisson y realizar un test de χ para comprobar si el fondo sigue una distribución de Poisson.

### Resultados:

- [x] a) Una vez construido el histograma, se anotan las frecuencias experimentales `f(B_j)` en una tabla, utilizando tantas filas como valores distintos de `B_j` se observen.

- [x] b) Se calcula su valor medio B y la desviación estándar muestral `s_B`.

- [x] c) Para cada valor de `B_j` se calcula la frecuencia predicha por la distribución de Poisson, `f_t(B_j) = NP_B(B_j)`, y se representa junto con su error (desviación estándar `σ[f_t(B_j)])` sobre el histograma de las medidas.

- [x] d) Se comprueba visualmente si ambas distribuciones son compatibles.

_Test χ² del histograma de frecuencia (o cómo no debe aplicarse el test χ²):_

- [x] e) Para tener un criterio cuantitativo estadístico de si los datos experimentales del fondo radiactivo siguen o no una distribución de Poisson, se recurre al test χ² . Para ello, se
construye el test estadístico:

`FÓRMULA 3`
`FÓRMULA 4`

- [x] f) Una vez hallado χ²_calculado, se determina el número de grados de libertad, ν=n–p−1, siendo p el número de parámet os deducidos de los datos que se han usado para hallar las frecuencias esperadas (en nuestro caso la media), y se obtiene el valor de χ² por grado de libertad, `χ_{ν}^{2,calculado} = (1/ν)*χ²_calculado`.

- [x] g) Por último, se halla la probabilidad de que repitiendo el experimento se encuentre un valor de `χ_ν^2` mayor que el calculado: `P[χ_ν^2 ≥ χ_ν^2,calculado]`. Si esta probabilidad se halla entre el 5% y 95%, se admite que la distribución experimental sigue una distribución de Poisson. Nótese que si el test estadístico χ calculado sigue correctamente una distribución de χ², el valor con máxima probabilidad corresponde a χ² calculado = ν.

_Test χ² del histograma de frecuencia con intervalos agrupados:_

- [x] h) Si la frecuencia esperada de un valor del fondo `B_j`, `NP_B(B_j)`, es menor que 5 (lo cual se presenta en las colas del histograma, véase la Figura 2.1), conviene agrupar este valor con los adyacentes, de tal manera que la frecuencia esperada para los valores agrupados `E_j` sea mayor o igual a 5. Se comparará esta frecuencia esperada con la correspondiente frecuencia observada (O_j) para la misma agrupación de intervalos. El valor del test estadístico será:

`FÓRMULA 5`

donde k es el número de intervalos agrupados que hayan resultado. Construir un nuevo histograma con esta agrupación de intervalos (incluyendo frecuencias observadas y 2 esperadas como en el caso anterior) y repetir el test de χ² siguiendo este procedimiento.



_Test χ² de la varianza:_

- [x] i) También se ha de verificar si la dispersión de los datos es compatible con la dispersión esperada en una distribución de Poisson. Para ello, se sigue el procedimiento descrito en el
apartado 3 (test de funcionamiento del sistema contador).

_Fondo radiactivo_

- [x] j) Por último, se establece el fondo radiactivo del tubo GM:

		FÓRMULA 6

## 3 - Test de funcionamiento del sistema contador

### Objetivo:

- Test estadístico para comprobar el correcto funcionamiento del contador Geiger-Mueller (tubo y electrónica).

### Resultados:

- [ ] a) Se efectúa el test χ² de la varianza para las 20 medidas.
- [ ] b) Se realiza el test χ² de la varianza para los datos de fondo del apartado 2.
- [ ] c) Conclusiones respecto al buen funcionamiento del contador GM.

## 4 - Espectro β y Plot de Kurie del ⁹⁰Sr/Y con un espectrómetro magnético

### Objetivo:

- Medida del espectro continuo β del ⁹⁰Sr/Y por medio de una técnica de espectrometría magnética y determinación de la energía de punto final del espectro del ⁹⁰Y. Construcción del Plot
de Kurie y comprobación experimental de la teoría de Fermi de la desintegración β.

### Resultados:

- [ ] a) ¿Qué conclusiones pueden extraerse de las medidas realizadas en los apartados 4 y 5?
- [ ] b) Representar gráficamente el espectro neto (con substracción de fondo) de cuentas/minuto frente a θ, r(θ) según la notación introducida previamente. Asignar como error del ángulo σ_θ = 2.5º.
- [ ] c. Realizar la calibración del espectrómetro magnético, utilizando las relaciones (8) y (9). Deberá obtenerse una curva de calibración similar a la de la Figura 4.3.
- [ ] d. Con la calibración obtenida, representar el espectro de cuentas netas/minuto frente a T, r(T).
- [ ] e. Dado que la relación entre T y θ no es lineal, los intervalos uniformes en θ con los que se han realizado las medidas no lo son en T. Por ello es necesario corregir las tasas de cuentas
netas dividiéndolas por el factor

		FÓRMULA 15

	Esta corrección es también necesaria para la correcta comparación del espectro medido al espectro beta teórico, que es función de T, por lo que puede también entenderse como el Jacobiano de la transformación. Para estimar los intervalos de error sobre las tasas corregidas, considerar dT / d θ como constante. El error sobre T vendrá dado por la relación σ_T = |dT/dθ|·σ_θ . Representar el espectro experimental corregido;
	
		r'(T) = r(T)/|dT/dθ|.
	
- [ ] f) Calcular y representar el correspondiente Plot de Kurie experimental utilizando la expresión (13) –reemplazando r(T) por r’(T)−, asumiendo un factor de forma S(p,q) = 1 (transición permitida). El error de f(T) se evaluará propagando la incertidumbre sobre la tasa de cuentas netas corregidas, r’(T), asumiendo constantes el resto de factores (dado que dichos factores sólo dependen de T, cuya incertidumbre aparece reflejada en la variable independiente):

`σ_f = |df/dr'|·σ_r' = (f·σ_r')/(2r')`
	
- [ ] g) Realizar un ajuste lineal a la parte central del Plot de Kurie y obtener la energía del punto
final del espectro beta (Q).
- [ ] h) Estimar los errores sistemáticos en la determinación de Q asociados al campo magnético B y el radio efectivo R (tomar como incertidumbre ≈0.01 T y ≈1 mm, respectivamente).

## 5 - Ajuste preciso del espectro β y Plot de Kurie del ⁹⁰Sr/Y

### Objetivos:

- Ajuste del espectro β y Plot de Kurie considerando la resolución angular del espectrómetro magnético y el nivel de prohibición de la desintegración β de 90 Y.

### Resultados:

- [ ] a.1) Realizar un ajuste del espectro experimental corregido al espectro teórico convolucionado, dado por la relación (16), para valores T>0.6 MeV y asumiendo que se trata de una transición primera prohibida. Los parámetros libres serán Q, la constante C y el parámetro σ de la resolución angular (tomar como valor inicial 10º). Para realizar este ajuste puede utilizarse el template ROOT plotKurie_fit.C, suministrado en el material de laboratorio de la asignatura, del que sólo será necesario modificar la configuración 2 y los datos de entrada 3, a través del fichero Sr-90_fit.dat. 
- [ ] a.2) Obtener el test estadístico de χ² del ajuste.
- [ ] b.1) Repetir el ajuste del espectro experimental corregido, asumiendo en este caso una transición permitida. 
- [ ] b.2) Comparar el valor del χ² de este ajuste al nominal del apartado anterior. 
- [ ] b.3) ¿Es posible discriminar entre transición permitida y transición primera prohibida? 
- [ ] c) Representar el correspondiente Plot de Kurie experimental y teórico, utilizando el espectro corregido y los valores de Q y σ obtenidos en el ajuste del espectro. Ambas representaciones se obtienen automáticamente mediante el template plotKurie_fit.C. 
- [ ] d) Estimar los errores sistemáticos en la determinación de Q siguiendo las prescripciones del apartado 
- [ ] e) Discusión de los resultados.

## 6 - Tiempo de resolución del contador Geiger-Mueller

### Objetivos:

- Deducción del tiempo muerto del detector.

### Resultados:

- [ ] a) Rellenando la Tabla 5.1, se calcula el error de cada una de las 3 medidas y de la medida suma por propagación directa de errores en la expresión completa (σ_τ^(i)) y la expresión aproximada (σ_{τ,aprox}^(i)) de la ecuación (8). En este último caso se calcula el error del numerador y denominador de la expresión (8) simplificada por propagación directa de los errores y usaremos errores relativos para el cálculo del error del cociente. Es importante calcular directamente en el laboratorio, al menos, los valores de τ(i) para las 3 medidas y la medida suma.
- [ ] b) Calcular el valor medio pesado (τ_w) con su error de las 3 medidas de la tabla (columnas sombreadas).
- [ ] c) Calcular el valor medio (sin pesar, τ ), la desviación estándar y la desviación estándar de la media de las 3 medidas (columnas sombreadas).
- [ ] d) Representar gráficamente las 3 medidas, el valor medio pesado y el valor medio, con sus correspondientes errores. ¿Qué conclusiones se pueden extraer a partir de la comparación de todos estos resultados?
- [ ] e) Determinar el valor de τ , que se considere más apropiado, y su error.
- [ ] f) Comparar el valor obtenido con el especificado por el fabricante.

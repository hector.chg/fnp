#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 17:02:02 2019

@author: hector
"""

# %% import librerías
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial
from scipy import odr

# %% Workspace
wspace = "/home/hector/Dropbox/Compartidos/INDIVIDUALES/"
wspace = wspace + "Esteban Rubio/fnp/p2/"
datadir = wspace + "datos/"
resultsdir = wspace + "resultados/"
figdir = resultsdir + "plots/"

# %% funciones


def leer(file_to_read, skipheader=3, delimiter=", "):
    try:
        with open(file_to_read, "r") as opened_file:
            file = np.genfromtxt(opened_file,
                                 skip_header=skipheader,
                                 delimiter=delimiter)
    except IOError as e:
        print("Operation failed: %s" % e.strerror)
    return file


def poisson(x, lamb):
    """poisson pdf, parameter lamb is the fit parameter"""
    return (lamb**x/factorial(x))*np.exp(-lamb)


def poisson2(x, lamb, norm):
    """poisson pdf, parameter lamb is the fit parameter"""
    return norm*(lamb**x/factorial(x))*np.exp(-lamb)


def poisson4odr1(params, x):
    lamb = params[0]
    return (lamb**x/factorial(x)) * np.exp(-lamb)


def poisson4odr2(params, x):
    lamb = params[0]
    norm = params[1]
    return norm*(lamb**x/factorial(x))*np.exp(-lamb)

# % parte 1 - hecho todo en excel

# %% parte 2 - fondo del detector


data = np.array([[0, 17],
                 [1, 27],
                 [2, 34],
                 [3, 33],
                 [4, 20],
                 [5, 11],
                 [6, 5],
                 [7, 2],
                 [8, 1],
                 [9, 0]])

partial = sum(data[:, 1])
norm_data = data[:, 1]/partial

# plot poisson-deviation with fitted parameter
x_plot = np.linspace(min(data[:, 0]), max(data[:, 0]), 100)

initial_guess2 = [3, 0.5]
# ajuste odr 1
model1 = odr.Model(poisson4odr1)
mydata = odr.RealData(data[:, 0], norm_data)
initial_guess = initial_guess2[1]*np.ones((1))
myodr = odr.ODR(mydata, model1, initial_guess)
results = myodr.run()
results.pprint()
parameter_name = ["mu"]
fit_parameters = np.zeros((1, 2))
fit_parameters[0, 0] = results.beta[0]
fit_parameters[0, 1] = np.sqrt(results.cov_beta[0, 0])
print("%s:" % parameter_name[0],
      fit_parameters[0, 0], "+-", fit_parameters[0, 1],
      "")
r2 = results.sum_square_delta/results.sum_square
print("r²: ", r2, "\n")

# ajuste odr 2
model2 = odr.Model(poisson4odr2)
myodr = odr.ODR(mydata, model2, initial_guess2)
results2 = myodr.run()
results2.pprint()
parameter_names = ["mu",
                   "Constante de normalización"]
fit_parameters2 = np.zeros((len(initial_guess2), 2))
for i in range(len(initial_guess2[:])):
    fit_parameters2[i, 0] = results2.beta[i]
    fit_parameters2[i, 1] = np.sqrt(results2.cov_beta[i, i])
    print("%s:" % parameter_names[i],
          fit_parameters2[i, 0], "+-", fit_parameters2[i, 1])
r2_2 = results2.sum_square_delta/results2.sum_square
print("r²: ", r2_2)

# plot poisson-deviation with fitted parameter
fig1 = plt.figure(1, figsize=(7, 7))
plt.suptitle("Frecuencia relativa de la variable discreta de fondo",
             fontsize=16)
plt.title("Variable normalizada para poder realizar el ajuste a una pdf de poisson", fontsize=10)

plt.plot(x_plot, poisson(x_plot, fit_parameters[0, 0]), '-',
         label=
         """scipy's odr fit to a Poisson pdf
  %s: %1.0f $\pm$ %1.0f
  r²: %1.5f
          """
          % (parameter_names[0], fit_parameters[0, 0], fit_parameters[0, 1],
             r2)
          )
# plt.plot(x_plot, poisson2(x_plot, fit_parameters2[0, 0], fit_parameters2[0, 0]),
         # 'b-', label="odr2 fit")
# plt.plot(x_plot, initial_guess2[0]*poisson(x_plot, initial_guess2[1]),
#          "k--", label="bruteforce")
# true data
# plt.plot(data[:, 0], norm_data, "g-")
plt.plot(data[:, 0], norm_data, "gx", label="experimental observations")

plt.xlabel("Number of counts")
plt.ylabel("normalized relative frequency")
plt.legend(loc="best")
plt.grid()
fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig(figdir+"poissonian_noise.png")
plt.show(fig1)

# %% parte 3 -
# %% parte 4 -
# %% parte 5 -
# %% parte 6 -